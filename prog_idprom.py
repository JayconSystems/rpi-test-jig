import sys
from time import sleep

from smbus import SMBus

BUS_NUM = 1
EEPROM_ADDR = 0x50
PAGE_SIZE = 8
IDPROM_FILE = "/home/pi/idprom.bin"

bus = SMBus(BUS_NUM)

with open(IDPROM_FILE, "rb") as f:
    idprom = f.read()
    f.seek(0)
    chunk = f.read(PAGE_SIZE)
    count = 0
    while chunk:
        print("Writing: ", " ".join("{:02x}".format(a) for a in list(chunk)))
        bus.write_i2c_block_data(EEPROM_ADDR, count * PAGE_SIZE, list(chunk))
        chunk = f.read(PAGE_SIZE)
        sleep(0.005)
        count += 1

    # Reset eeprom address to 0x00
    bus.write_byte(EEPROM_ADDR, 0x00)
    # Verify that the contents just written
    read_data = []
    count = 0
    for ref in idprom:
        act = bus.read_byte(EEPROM_ADDR)
        #print("{} ".format(act))
        if ref != act:
            print("Verification failed at {} ({} != {})".format(count, act, ref))
            sys.exit(1)
        read_data.append(act)
        if (len(read_data) % PAGE_SIZE) == 0:
            print("Read:    ", " ".join("{:02x}".format(a) for a in read_data))
            read_data = []
        count += 1
    if read_data:
        print("Read:    ", " ".join("{:02x}".format(a) for a in read_data))
    print("Verified: OK")
    sys.exit(0)
