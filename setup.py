from setuptools import find_packages, setup

setup(name='rpijig',
      version='0.1.0',
      package_dir={"": "src"},
      packages=find_packages(where='src'),
      author="Shawn Nock",
      author_email="shawn@monadnock.ca",
      description="Framework for using Raspberry Pi to control PCB test fixtures",
      license="APLv2",
      keywords="",
      install_requires=['twisted', 'sysfs-gpio',
                        'enum34', 'pyserial', 'attrs',
                        'smbus-cffi'],
      include_package_data=True)
