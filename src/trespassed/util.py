"""Utility Functions for Trespassed."""
from twisted.internet import reactor, task


def delay(sec):
    """Return a deferred that fires `sec` seconds in the future."""
    return task.deferLater(reactor, sec, lambda: None)


skip = True


def quit(*args, **kwargs):
    """End the Trespassed application."""
    global skip
    if not skip:
        reactor.stop()
    skip = False
