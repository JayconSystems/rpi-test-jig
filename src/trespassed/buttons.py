import time

from functools import wraps

from twisted.internet import defer
from twisted.python import log

from trespassed.gpio import register_handler

def debounce(f):
    @wraps(f)
    def wrapper(self, *args, **kwargs):
        now = time.monotonic()
        delta = now - self.LAST
        print(delta)
        self.LAST = now
        if delta > self.DEBOUNCE_SECONDS:
            return f(self, *args, **kwargs)
        return None
    return wrapper

def if_enabled(f):
    @wraps(f)
    def wrapper(self, *args, **kwargs):
        if self.enabled:
            return f(self, *args, **kwargs)
        return None
    return wrapper

class Buttons(object):
    def __init__(self, button_list):
        self.LAST = time.monotonic()
        self.DEBOUNCE_SECONDS = 2
        self.enabled = False

        for b in button_list:
            register_handler(b, self._do_button)

        self.d = {}

    def enable(self):
        log.msg("Buttons Enabled")
        self.enabled = True

    @debounce
    @if_enabled
    def _do_button(self, number, state):
        if number in self.d:
            d = self.d[number]
            del self.d[number]
            d.callback(None)
        log.msg("Button on {}".format(number))

    def wait_for(self, number):
        if number in self.d:
            d = self.d[number]
        else:
            d = defer.Deferred()
            self.d[number] = d
        return d
