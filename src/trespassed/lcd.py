"""HD44780 LCD driver for Trespassed."""
import time

from smbus import SMBus

from twisted.internet import reactor, defer
from twisted.python import log

from trespassed.gpio import OUTPUTS, set_output
from trespassed.util import delay

__all__ = ('lcd_write_status')

ADDR = 0x20
BUS = 1

LCD_DATA = 1
LCD_CMD = 0

LCD_LINE_ADDR = {0: 0x0,
                 1: 0x40}

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005

LCD_WIDTH = 16

# HD44780 Commands
LCD_CLEAR = 1
LCD_HOME = (1 << 1)

LCD_ENTRY_MODE = (1 << 2)
CURSOR_INCR = (1 << 1)
CURSOR_DECR = (0 << 1)
SHIFT_ENABLE = (1 << 0)
SHIFT_DISABLE = (0 << 0)

DISPLAY_CTRL = (1 << 3)
DISPLAY_ON = (1 << 2)
DISPLAY_OFF = (0 << 2)
CURSOR_ON = (1 << 1)
CURSOR_OFF = (0 << 1)
BLINK_ON = (1 << 0)
BLINK_OFF = (0 << 0)

FUNC_SET = (1 << 4)
DATA_4BIT = (1 << 3)
DATA_8BIT = (0 << 3)
TWO_LINES = (1 << 2)
ONE_LINE = (0 << 2)

SET_ADDRESS = (1 << 7)

class LCD(object):

    @defer.inlineCallbacks
    def _init_hd44780(self):
        log.msg("Starting LCD Initialization")
        # Initialize, set to 4bit mode
        yield self._write4(0x3)
        # Again, this time with feeling!
        yield self._write4(0x3)
        # Again, this time with feeling!
        yield self._write4(0x3)
        yield self._write4(0x2)

        # Now we're in 4bit mode, move up to normal commands
        yield self.write_cmd(FUNC_SET | TWO_LINES | DATA_4BIT)  # 2 lines, 4bit
        yield self.write_cmd(LCD_CLEAR)  # Clear Display
        yield self.write_cmd(LCD_HOME)  # Home Display
        yield self.write_cmd(DISPLAY_CTRL | DISPLAY_ON)  # Display ON
        yield self.write_cmd(LCD_ENTRY_MODE | CURSOR_INCR)  # Entry Mode
        log.msg("LCD Initialized")

    @defer.inlineCallbacks
    def _write4(self, nibble, data=False):
        yield defer.DeferredList([
            set_output(OUTPUTS.LCD_RS, 1 if data else 0),
            set_output(OUTPUTS.LCD_D4, nibble & 0x1),
            set_output(OUTPUTS.LCD_D5, nibble & 0x2),
            set_output(OUTPUTS.LCD_D6, nibble & 0x4),
            set_output(OUTPUTS.LCD_D7, nibble & 0x8)])
        yield self._latch()

    @defer.inlineCallbacks
    def write_cmd(self, byte, data=False):
        # Shifts the bytes up, DB* macros don't work
        high = (byte & 0xf0) >> 4
        low = byte & 0x0f
        yield self._write4(high, data)
        yield self._write4(low, data)

    def _write_data(self, byte):
        return self.write_cmd(byte, data=True)

    @defer.inlineCallbacks
    def _latch(self):
        yield set_output(OUTPUTS.LCD_E, 0)
        time.sleep(E_PULSE)
        yield set_output(OUTPUTS.LCD_E, 1)
        time.sleep(E_DELAY)

    @defer.inlineCallbacks
    def write(self, line, string):
        yield self.write_cmd(SET_ADDRESS | LCD_LINE_ADDR[line])
        string = string.ljust(LCD_WIDTH, " ")[:LCD_WIDTH]
        for i in range(LCD_WIDTH):
            yield self._write_data(ord(string[i]))

    def shutdown(self):
        return self.write_cmd(DISPLAY_CTRL | DISPLAY_OFF)


LCD_SINGLETON = LCD()

@defer.inlineCallbacks
def lcd_write_status(status):
    """Write the 16x2 status string to the display."""
    yield LCD_SINGLETON.write_cmd(LCD_CLEAR)  # Clear Display
    yield LCD_SINGLETON.write_cmd(LCD_HOME)
    lines = status.split('\n')[:2]
    for i in range(len(lines)):
        LCD_SINGLETON.write(i, lines[i])

@defer.inlineCallbacks
def lcd_init():
    yield LCD_SINGLETON._init_hd44780()


#reactor.addSystemEventTrigger('before', 'shutdown',
#                             LCD_SINGLETON.shutdown)
