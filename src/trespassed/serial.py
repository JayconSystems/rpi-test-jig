"""Serial Bus fuctionality for Trespassed."""

import re

from enum import IntEnum
from time import sleep

import attr
import six

from automat import MethodicalMachine

from twisted.application import service
from twisted.application.internet import TimerService
from twisted.internet import defer, protocol, reactor
from twisted.internet.serialport import SerialPort
from twisted.protocols.basic import LineReceiver
from twisted.python import log

from trespassed.lcd import lcd_write_status
from trespassed.util import delay




__all__ = ('SerialService', 'ConsoleProtocol', 'send_command')

@attr.s
class Matcher(object):
    regex = attr.ib()
    callback = attr.ib()

    def run(self, line):
        if self.regex.search(line):
            print(self.regex.search(line))
            return self.callback(line)
        return False

class ConsoleProtocol(LineReceiver):
    def __init__(self, *args, **kwargs):
        self.delimiter = b"\n"
        self._handlers = []
        self._log = []
        LineReceiver.__init__(self, *args, **kwargs)

    #@defer.inlineCallbacks
    def lineReceived(self, line):
        line = line.strip(b"\r")
        print(line)
        for m in self._handlers:
            m.run(line)
        self._log.append(line)

    def get_log(self):
        return self._log

    def clear_log(self):
        self._log = []

    def sendLine(self, *args, **kwargs):
        print("Tried to send: {}".format(args))
        LineReceiver.sendLine(self, *args, **kwargs)

    def register(self, regex, callback):
        h = Matcher(re.compile(regex), callback)
        self._handlers.append(h)


class SerialService(service.Service):
    """Service wrapper for twisted.internet.serialport."""

    _serial = None  # type: SerialPort
    _port = None    # type: str

    def __init__(self, proto, port='/dev/ttyS0', baudrate=115200):
        # type: (str, int) -> None
        """Instanatiate SerialPort handling service."""
        self._port = port
        self._baud = baudrate
        self.proto = proto

    def startService(self):
        """Start the NBus serial handling service."""
        log.msg("SerialService Started")
        try:
            self._serial = SerialPort(self.proto, self._port,
                                      reactor, baudrate=self._baud)
        except IOError as e:
            log.msg(
                "Failed to initialize serial: {}".format(e))

    def stopService(self):
        """Stop the NBus serial handling service."""
        self._serial.loseConnection()
