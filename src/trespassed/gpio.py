"""Local GPIO functions."""
from enum import IntEnum

from sysfs.gpio import (Controller, INPUT, OUTPUT, RISING, FALLING)

from trespassed.util import quit, delay

from twisted.internet import defer, reactor
from twisted.python import log

__all__ = ('set_output', 'get_output', 'register_handler')

PINS = {}


class OUTPUTS(IntEnum):
    EM_POWER = 4
    LCD_E = 22
    LCD_RS = 27
    LCD_D4 = 5
    LCD_D5 = 6
    LCD_D6 = 23
    LCD_D7 = 17


class INPUTS(IntEnum):
    BUTTON3 = 26
    BUTTON2 = 19
    BUTTON1 = 13


Controller.available_pins = [ INPUTS.BUTTON1, INPUTS.BUTTON2,
                              INPUTS.BUTTON3, OUTPUTS.EM_POWER, OUTPUTS.LCD_E, OUTPUTS.LCD_RS,
                              OUTPUTS.LCD_D4, OUTPUTS.LCD_D5, OUTPUTS.LCD_D6, OUTPUTS.LCD_D7]


@defer.inlineCallbacks
def set_output(pin_no, level):
    """Set the output to the specified value."""
    if pin_no not in PINS:
        try:
            pin = yield Controller.alloc_pin(pin_no, OUTPUT)
            PINS[pin_no] = pin
        except Exception as e:
            log.msg("Failed to allocate {}: {}".format(pin_no, e))
            defer.returnValue(None)
    pin = PINS[pin_no]
    if level:
        #log.msg("Setting {}".format(pin_no))
        pin.set()
    else:
        #log.msg("Clearing {}".format(pin_no))
        pin.reset()


def get_output(pin_no):
    """Return the current value (bool) of the output."""
    pin = PINS[pin_no]
    return pin.read()


@defer.inlineCallbacks
def register_handler(i, function):
    yield Controller.alloc_pin(i, INPUT, function, FALLING)
    log.msg("Registered {} = {}".format(i, function.__name__))
