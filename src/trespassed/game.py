"""Game definition file for Trespassed."""
import os
import time

from functools import wraps

from automat import MethodicalMachine

from twisted.internet import defer, protocol

from trespassed.buttons import Buttons
from trespassed.gpio import INPUTS, OUTPUTS, set_output
from trespassed.lcd import lcd_init, lcd_write_status
from trespassed.util import delay

from twisted.internet import reactor
from twisted.python import log

defer.setDebugging(True)

class ExitOnlyPP(protocol.ProcessProtocol):
    def __init__(self):
        self.log = []
        self.d = defer.Deferred()

    def outReceived(self, data):
        self.log += data.split(b"\n")

    def errReceived(self, data):
        self.log += data.split(b"\n")

    def processExited(self, reason):
        self.d.callback(reason.value.exitCode)

class EMProgStateMachine(object):
    _machine = MethodicalMachine()

    def __init__(self, buttons, proto):
        self._buttons = buttons
        self._programmed = False
        self._tested = False
        self._working_ble = False
        self._working_network = False
        self._em_serial = b""
        self.proto = proto
        INPUT_MAP = [(b'Please choose the operation:', self.saw_uboot_menu),
                     (b'^VoCore2 > ', self.saw_uboot_prompt),
                     (b'Please press Enter to activate this console.', self.saw_shell_ready),
                     (b'c3listener: Acquired ibeacon maj=50115 min=50115', self.saw_working_ble),
                     (b'udhcpc: lease of [0-9.]+ obtained', self.saw_working_network),
                     (b'EM Serial Number: [0-9a-f]{10}', self.saw_serial_number),
        ]
        for i in INPUT_MAP:
            self.proto.register(*i)

    def wait_for_button(self, button):
        return self._buttons.wait_for(button)

    @_machine.state(initial=True)
    def _init(self):
        """ Unknown EM state. """
        pass

    @_machine.state()
    def uboot_menu(self):
        """ Boot Menu presented for 10 sec. at boot """
        pass

    @_machine.input()
    def saw_uboot_menu(self, line):
        pass

    @_machine.input()
    def saw_uboot_prompt(self, line):
        pass

    @_machine.state()
    def uboot_prompt(self):
        """ UBoot Command Line. """
        pass

    @_machine.input()
    def saw_shell_ready(self, line):
        pass

    @_machine.state()
    def shell_ready(self):
        pass

    @_machine.input()
    def saw_working_ble(self, line):
        pass

    @_machine.input()
    def saw_working_network(self, line):
        pass

    @_machine.input()
    def saw_serial_number(self, line):
        pass

    @defer.inlineCallbacks
    @_machine.output()
    def _do_uboot_menu(self, line):
        print("UBOOT MENU", self._programmed)
        if self._programmed and not self._tested:
            self.proto.transport.write(b"3")
            return defer.returnValue(None)

        self.proto.transport.write(b"4")
        yield delay(1)
        self.proto.transport.write(b"\n")

    def clear(self):
        self._programmed = False
        self._tested = False
        self._working_ble = False
        self._working_network = False
        self._em_serial = b""
        self.proto.clear_log()

    @defer.inlineCallbacks
    @_machine.output()
    def _do_uboot_prompt(self, line):
        if self._tested:
            if not self._em_serial:
                lcd_write_status("Insert board\nSerial: FAIL")
            else:
                try:
                    with open('/home/pi/logs/{}.log'.format(self._em_serial.decode()), 'wb') as logfile:
                        logfile.write(b"\n".join(self.proto.get_log()))
                except Exception as e:
                    lcd_write_status("Insert board\nLog: FAIL")
                    log.msg("Error writing log: {}".format(e))
                    self.clear()
                    defer.returnValue(None)
            if self._working_ble and self._working_network:
                lcd_write_status("Insert board\nUnit OK")
            elif not self._working_ble:
                lcd_write_status("Insert board\nBLE: FAIL")
            else:
                lcd_write_status("Insert board\nNetwork: FAIL")
            self.clear()

        elif not self._programmed:
            # Program IDPROM
            lcd_write_status("Prog IDPROM")
            pp1 = ExitOnlyPP()
            reactor.spawnProcess(pp1, "/home/pi/rpi-test-jig/.env/bin/python", ["/home/pi/rpi-test-jig/.env/bin/python", "/home/pi/rpi-test-jig/prog_idprom.py"], env={"VIRTUAL_ENV": "/home/pi/rpi-test-jig/.env"})
            status = yield pp1.d
            for line in pp1.log:
                self.proto.lineReceived(line)
            if status == 0:
                lcd_write_status("Prog IDPROM\nVerify: PASS")
                #yield delay(2)
            else:
                lcd_write_status("IDPROM Prog Fail\nInsert Board")
                defer.returnValue(None)

            # Program nRF52
            pp = ExitOnlyPP()
            reactor.spawnProcess(pp, "/usr/bin/sudo", ["sudo", "/home/pi/openocd-code/src/openocd", "-s", "tcl", "-f", "program-ble.openocd"], path="/home/pi/openocd-code/")
            lcd_write_status("Program nRF52")
            log.msg("Programming nRF52")
            status = yield pp.d
            for line in pp.log:
                self.proto.lineReceived(line)
            if status == 0:
                yield lcd_write_status("Program nRF52\nPASS")
                #yield delay(1)
            else:
                self.proto.lineReceived('OpenOCD failed with code {}'.format(status).encode())
                yield lcd_write_status("Program nRF52\nFAIL")
                #yield delay(1)
            self._programmed = True
            lcd_write_status("Booting Module")
            self.reboot(None)
        else:
            self.reboot(None)

    @defer.inlineCallbacks
    @_machine.output()
    def _do_shell(self, line):
        self.proto.sendLine(b"") # CR to activate console
        # Do Tests
        lcd_write_status("Testing...",)
        self.proto.sendLine(b"ip addr show dev eth0 | awk -F\"[:\ ]\" '/link\/ether/ {printf(\"EM Serial Number: %s%s%s%s%s\\n\", $7,$8,$9,$10,$11)}'")
        self.proto.sendLine(b"logread -f &")
        yield delay(90)
        self._tested = True
        self.reboot(None)

    @_machine.input()
    def reboot(self, line):
        pass

    @_machine.output()
    def _do_uboot_menu_reboot(self, line):
        print("REBOOTING FROM UBOOT MENU")
        self.proto.sendLine(b"4")
        self.proto.sendLine(b"reset")

    @_machine.output()
    def _do_uboot_reboot(self, line):
        print("REBOOTING FROM UBOOT PROMPT")
        self.proto.sendLine(b"reset")

    @_machine.output()
    def _do_shell_reboot(self, line):
        print("REBOOTING FROM SHELL")
        self.proto.sendLine(b"reboot")

    @_machine.output()
    def _try_all_reboots(self, line):
        print("REBOOT ALL THE THINGS!")
        self.proto.sendLine(b"4")
        self.proto.sendLine(b"reset")
        self.proto.sendLine(b"reboot")

    @defer.inlineCallbacks
    @_machine.output()
    def _ble_test_success(self, line):
        self._working_ble = True
        self._tested = True
        yield delay(1)
        self.reboot(None)

    @_machine.output()
    def _network_test_success(self, line):
        self._working_network = True

    @_machine.output()
    def _collect_serial_number(self, line):
        self._em_serial = line.split(b" ")[3]


    _init.upon(saw_uboot_menu, enter=uboot_menu, outputs=[_do_uboot_menu])
    _init.upon(saw_uboot_prompt, enter=uboot_menu, outputs=[_do_uboot_prompt])
    _init.upon(saw_shell_ready, enter=shell_ready, outputs=[_do_shell])
    _init.upon(reboot, enter=_init, outputs=[_try_all_reboots])

    uboot_menu.upon(saw_uboot_prompt, enter=uboot_prompt, outputs=[_do_uboot_prompt])
    uboot_menu.upon(saw_shell_ready, enter=shell_ready, outputs=[_do_shell])
    uboot_menu.upon(reboot, enter=_init, outputs=[_do_uboot_reboot])

    uboot_prompt.upon(saw_uboot_menu, enter=uboot_menu, outputs=[_do_uboot_menu])
    uboot_prompt.upon(saw_shell_ready, enter=shell_ready, outputs=[_do_shell])
    uboot_prompt.upon(saw_uboot_prompt, enter=uboot_prompt, outputs=[])
    uboot_prompt.upon(reboot, enter=_init, outputs=[_do_uboot_reboot])

    shell_ready.upon(reboot, enter=_init, outputs=[_do_shell_reboot])
    shell_ready.upon(saw_working_ble, enter=shell_ready, outputs=[_ble_test_success])
    shell_ready.upon(saw_working_network, enter=shell_ready, outputs=[_network_test_success])
    shell_ready.upon(saw_serial_number, enter=shell_ready, outputs=[_collect_serial_number])

@defer.inlineCallbacks
def game_init(application):
    from trespassed.serial import SerialService, ConsoleProtocol

    buttons = Buttons([INPUTS.BUTTON1, INPUTS.BUTTON2, INPUTS.BUTTON3])
    buttons.enable()

    proto = ConsoleProtocol()
    SM = EMProgStateMachine(buttons, proto)
    serial_service = SerialService(proto)
    serial_service.setServiceParent(application)

    yield lcd_init()
    lcd_write_status("Insert Board")
