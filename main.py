"""Main endpoint for trespassed application."""

import RPi.GPIO as GPIO

from trespassed.serial import SerialService

from trespassed.game import game_init

from twisted.application.service import Application
from twisted.internet import reactor

# Enable pullups since Manny left them out
GPIO.setmode(GPIO.BCM)
GPIO.setup(26, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(19, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(13, GPIO.IN, GPIO.PUD_UP)

application = Application("RPi Test")

reactor.addSystemEventTrigger('after', 'startup', game_init, application)
